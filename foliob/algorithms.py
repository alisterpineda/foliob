import copy
import math
from abc import ABC, abstractmethod
from typing import Optional, Tuple


class AlgorithmBase(ABC):

    def __init__(self, buy_only: bool = False):
        self.buy_only = buy_only

    @abstractmethod
    def run(self, account: 'foliob.core.Account'):
        pass


class BasicAlgorithm(AlgorithmBase):
    def run(self, account: 'foliob.core.Account'):
        # TODO warn about no buy_only feature
        total_val = account.market_value()
        total_weight = account.total_weight()

        cash_reserve = int(total_val * account.cash_weight / total_weight)
        cash_avail = account.cash - cash_reserve
        for pos in account.positions:
            target_val = total_val * (pos.weight / total_weight)
            target_amount = math.floor(target_val / pos.security.price)
            pos.delta = target_amount - pos.amount
            cash_avail -= pos.delta * pos.security.price
        account.cash_delta = cash_reserve + cash_avail - account.cash


class GreedyAlgorithm(AlgorithmBase):

    @staticmethod
    def get_next_step(account: 'foliob.core.Account', buy_only: bool = False) -> Optional[Tuple[str, int]]:
        possible_steps = {}

        for pos in account.positions:
            acc_buy = copy.deepcopy(account)
            ps = next(x for x in acc_buy.positions if x.security.symbol == pos.security.symbol)
            if acc_buy.cash + acc_buy.cash_delta > ps.security.price:
                ps.delta += 1
                acc_buy.cash_delta -= ps.security.price * 1
                possible_steps[(ps.security.symbol, 1)] = acc_buy.score()

            if not buy_only:
                acc_sell = copy.deepcopy(account)
                ps = next(x for x in acc_sell.positions if x.security.symbol == pos.security.symbol)
                if ps.amount > 0:
                    ps.delta -= 1
                    acc_sell.cash_delta -= ps.security.price * -1
                    possible_steps[(ps.security.symbol, -1)] = acc_sell.score()

        if not possible_steps:
            return None

        lowest_score_step = min(possible_steps, key=possible_steps.get)

        if possible_steps[lowest_score_step] < account.score():
            return lowest_score_step
        else:
            return None

    def run(self, account: 'foliob.core.Account'):
        while True:
            next_step = self.get_next_step(account, self.buy_only)
            if next_step is None:
                break

            sym = next_step[0]
            dd = next_step[1]

            pos = next(pos for pos in account.positions if pos.security.symbol == sym)
            if dd > 0 or dd < 0:
                pos.delta += dd
                account.cash_delta -= pos.security.price * dd
            else:
                raise ValueError

