import certifi
import urllib3
import json
import time
from abc import ABC, abstractmethod
from collections import namedtuple

import foliob.core

ApiQueryResult = namedtuple('ApiQueryResult', ['price', 'bid', 'ask'])
ApiQueryResult.__new__.__defaults__ = (None, None, None)


class ApiBase(ABC):
    def __init__(self, api_key: str):
        self.api_key = api_key

    def update_security(self, security: 'foliob.core.Security'):
        result = self.submit_query(security.symbol)
        security.price = result.price
        security.bid = result.bid
        security.ask = result.ask

    def update_securities(self, portfolio: 'foliob.core.Portfolio'):
        for sec in portfolio.securities:
            self.update_security(sec)

    @abstractmethod
    def submit_query(self, symbol: str):
        pass


class AlphaVantage(ApiBase):
    def submit_query(self, symbol: str) -> ApiQueryResult:
        for attempt_num in range(3):
            try:
                url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={0:s}&apikey={1:s}'
                http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
                response = http.request('GET', url.format(symbol, self.api_key))
                data = json.loads(response.data.decode('utf8'))

                price = int(round(float(data['Global Quote']['05. price']) * 100))
                return ApiQueryResult(price)
            except KeyError:
                print("Exceeded API limit! Waiting 60s...")
                time.sleep(60)
        else:
            raise Exception("Something wrong with the API?")  # TODO reword
