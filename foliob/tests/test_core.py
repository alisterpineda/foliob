import pytest
import random

import foliob.tests.test_resources as test_resources
import foliob.core
import foliob.algorithms
import foliob.api


@pytest.fixture(scope='function')
def portfolio():
    folio = foliob.core.Portfolio()
    yield folio


@pytest.fixture(scope='class', params=test_resources.symbols)
def security_symbol(request):
    return request.param


class TestAccount(object):
    @pytest.mark.xfail
    def test_create_position_duplicate(self, portfolio, security_symbol):
        acc = portfolio.create_account()
        for s in foliob.tests.test_resources.symbols:
            portfolio.add_security(s)
            acc.create_position(s, random.randrange(1000), random.randrange(100))
        acc.create_position(security_symbol, 1)


class TestPortfolio(object):
    def test_add_security(self, portfolio, security_symbol):
        portfolio.add_security(security_symbol)

    @pytest.mark.xfail
    def test_add_security_duplicate(self, portfolio, security_symbol):
        portfolio.add_security(security_symbol)
        portfolio.add_security(security_symbol)

    def test_get_security(self, portfolio, security_symbol):
        portfolio.add_security(security_symbol)
        sec = portfolio.get_security(security_symbol)
        assert sec == foliob.core.Security(security_symbol)

    def test_remove_security(self, portfolio, security_symbol):
        portfolio.add_security(security_symbol)
        portfolio.remove_security(security_symbol)
        sec = portfolio.get_security(security_symbol)
        assert sec is None

    def test_create_account(self, portfolio):
        portfolio.create_account()
