import pytest

from foliob.api import *


class TestApi(object):

    @pytest.mark.parametrize('api_class,api_key,expect_bid,expect_ask', [
        pytest.param(AlphaVantage, '', False, False, id='alpha_vantage')
    ])
    def test_submit_query(self, api_class, api_key, expect_bid, expect_ask):
        api = api_class(api_key)
        result = api.submit_query('MSFT')

        assert result.price is not None
        if expect_bid:
            assert result.bid is not None
        if expect_ask:
            assert result.bid is not None
