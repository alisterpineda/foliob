import math
from typing import Callable, List, Optional, Union

import foliob.api


class Security(object):
    """This class represents a particular tangible asset that holds a monetary value.

    Specifically, this representation of an asset is typically purchased at a stock exchange and must have a ticker
    symbol attached to an object of this class.

    Attributes
    ----------
    symbol: str
        Ticker symbol. Used for retrieving quotes online.
    name: str, optional
        Name of this security.
    price: int
        The most recent price that this security was traded with.
    bid: int
        The price that buyers are willing to trade with.
    ask: int
        The price that sellers are willing to trade with.
    """

    def __init__(self, symbol: str):
        self.symbol: str = symbol.upper()
        self.name: str = None
        self.price: int = None
        self.bid: int = None
        self.ask: int = None

    def __repr__(self):
        return "<Security symbol:{0} price:{1} bid:{2} ask:{3}>".format(self.symbol, self.price, self.bid, self.ask)

    def __eq__(self, other):
        if isinstance(other, Security):
            return self.symbol == other.symbol
        else:
            return False

    def __hash__(self):
        return hash(self.symbol)


class Position(object):
    """This class represents the amount of a security owned by a certain account.

    This class also contains extra attributes that are used to balance accounts.

    Attributes
    ----------
    security: obj:`Security`
        The Security object that this object refers to.
    amount: int
        Amount of securities owned by this position.
    weight: int
        Used for calculations to balance an account. The higher the number, the higher the share of an account's total
        value is allocated to this position.
    delta: int
        The recommended amount to buy or sell (+ or -) as determined by a balancing algorithm.
    """

    def __init__(self, security: Security, amount: int, weight: int = None):
        self.security: Security = security
        self.amount: int = amount
        self.weight: int = weight
        self.delta: int = 0

    def market_value(self):
        return self.security.price * self.amount

    def market_value_new(self):
        return self.security.price * (self.amount + self.delta)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return str(self.__class__) + ": symbol:{},amount:{},weight:{}".format(self.security.symbol, self.amount, self.weight)


class Account(object):
    """ This class represents an investment account that contains positions and leftover cash.

    Attributes
    ----------
    portfolio: obj:`Portfolio`
        The Portfolio object that this object belongs to. Used as reference to get shared info like list of Security
        objects, etc.
    name: str, optional
        Name of this account.
    positions: obj:`List` of obj:`Position`
        List of positions that this account holds.
    cash: int
        Amount of cash this account holds.
    cash_weight: int or float
        Used for calculations to balance an account. The higher the number, the higher the share of an account's total
        value is allocated to cash.
    cash_delta: float
        The change in cash due to recommended buys and sells of securities by a balancing algorithm.
    """

    def __init__(self,
                 portfolio: 'Portfolio',
                 cash: int = 0,
                 name: str = "unnamed account",
                 cash_weight: int = 0):
        self.portfolio: 'Portfolio' = portfolio
        self.name: str = name

        self.positions: List[Position] = []

        self.cash: int = cash
        self.cash_weight: int = cash_weight
        self.cash_delta: int = 0

    def create_position(self, symbol: str, amount: int, weight: int = None) -> Position:
        for pos in self.positions:
            if pos.security.symbol == symbol:
                raise Exception("Position already exists for this symbol!")  # TODO reword
        p = Position(self.portfolio.get_security(symbol, True), amount, weight)
        self.positions.append(p)
        return p

    def market_value(self) -> int:
        market_val = self.cash
        for pos in self.positions:
            market_val += pos.market_value()
        return market_val

    def market_value_new(self) -> int:
        market_val_new = self.cash + self.cash_delta
        for pos in self.positions:
            market_val_new += pos.market_value_new()
        return market_val_new

    def total_weight(self) -> int:
        weight = self.cash_weight
        for pos in self.positions:
            weight += pos.weight
        return weight

    def score(self) -> float:
        sc = 0
        for pos in self.positions:
            alloc_new = pos.market_value_new() / self.market_value_new()
            alloc_tgt = pos.weight / self.total_weight()
            sc += abs(alloc_tgt - alloc_new)
        return sc


class Portfolio(object):
    def __init__(self):
        self.securities = []
        self.accounts = []

    def get_security(self, symbol: str, create: bool = False) -> Optional[Security]:
        for sec in self.securities:
            if sec.symbol == symbol:
                return sec
        if create:
            sec = self.add_security(symbol)
            return sec
        else:
            return None

    def add_security(self, symbol: str) -> Security:
        sec = Security(symbol)
        if sec not in self.securities:
            self.securities.append(sec)
            return sec
        else:
            raise ValueError("Security '{}' is already in the list!".format(symbol))

    def remove_security(self, symbol: str):
        sec = self.get_security(symbol)
        self.securities.remove(sec)

    def create_account(self,
                       cash: int = 0,
                       cash_weight: int = 0,
                       name: str = 'unnamed account') -> Account:
        acc = Account(self, cash, name, cash_weight)
        self.accounts.append(acc)
        return acc
