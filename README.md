# FolioB

## About

FolioB is a Python application that helps with balancing investment accounts by providing advice on what to buy and sell based on allocation targets.

**Current Features**

* Simple command-line interface
* Balance one account at a time using a simple balancing algorithm
* Retrieve stock quotes online (only Alpha Vantage for now)

**Possible Features**

* GUI
* Balance multiple accounts
* Portfolio-level allocation targets
* Multiple portfolios
* Add more complex balancing algorithms (next step: buy-only)
* Flexible ways to retrieve online quotes


## Instructions
### Getting FolioB
`git clone git@gitlab.com:alisterpineda/foliob.git`

**Dependencies**
* certifi
* prettytable
* urllib3

### Using Foliob
Inside the project folder, run the following to see the help prompt for the CLI program:

`./client.py -h`

Below is an example using the "Aggressive" option in the following link:
https://cdn.canadiancouchpotato.com/wp-content/uploads/2018/01/CCP-Model-Portfolios-ETFs-2017.pdf

```
$ python3 foliob_cli.py -c 250 <API KEY> vcn.to,200,30 xaw.to,500,60 zag.to,150,10
+--------+-------------+---------+------------------+-------------------------------------+--------------------------+
| Symbol |    Weight   |  Price  |      Amount      |                Value                |        Allocation        |
+--------+-------------+---------+------------------+-------------------------------------+--------------------------+
| VCN.TO | 30 (30.00%) | $ 28.10 |  200 -> 209 (9)  |  $ 5620.00 -> $ 5872.90 ($ 252.90)  | 28.67% -> 29.96% (1.29%) |
| XAW.TO | 60 (60.00%) | $ 22.87 | 500 -> 514 (14)  | $ 11435.00 -> $ 11755.18 ($ 320.18) | 58.34% -> 59.98% (1.63%) |
| ZAG.TO | 10 (10.00%) | $ 15.30 | 150 -> 128 (-22) |  $ 2295.00 -> $ 1958.40 ($ -336.60) | 11.71% -> 9.99% (-1.72%) |
|  CASH  |  0 (0.00%)  |   None  |       None       |   $ 250.00 -> $ 13.52 ($ -236.48)   | 1.28% -> 0.07% (-1.21%)  |
+--------+-------------+---------+------------------+-------------------------------------+--------------------------+
```
