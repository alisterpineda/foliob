#!/usr/bin/env python3
import argparse
import math
import sys
from prettytable import PrettyTable

from foliob.algorithms import *
from foliob.api import *
from foliob.core import *

algorithms = ['basic', 'greedy']
sources = ['alphavantage']

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Command-line interface for FolioB. Limited to a single account.")
    parser.add_argument('-b', action='store_true', dest="buy_only", help="If supported, the balancing algorithm will only consider buying, not selling.")
    parser.add_argument('--algorithm', type=str, default='basic', choices=algorithms, metavar='algorithm', help="Algorithm to be used to rebalance the account. Allowed values are: "+', '.join(algorithms))
    parser.add_argument('--api_key', type=str, default='0',help="API key to be used when retrieving stock quotes online.")
    parser.add_argument('--cash', type=float, default=0.0, metavar='cash', dest='cash', help="Cash stored in the account.")
    parser.add_argument('--source', type=str, default='alphavantage', choices=sources, metavar='source', help="Online source to retrieve stock quotes from. Allowed values are: "+', '.join(sources))
    parser.add_argument('--weight', type=int, default=0, metavar='weight', dest='cash_weight', help="Used to determine the target allocation for cash.")
    parser.add_argument('position', type=str, nargs='+', help="Formatted as 'symbol,amount,weight'. (Ex: AAPL,100,10)")
    args = parser.parse_args()

    cash = int(round(args.cash * 100))

    folio = Portfolio()
    acc = folio.create_account(cash, args.cash_weight)

    for pos in args.position:
        sym, amount, weight = pos.split(',')
        acc.create_position(sym, int(amount), int(weight))

    if args.source == 'alphavantage':
        api = AlphaVantage(args.api_key)
    else:
        raise NotImplementedError

    if args.algorithm == 'basic':
        alg = BasicAlgorithm()
    elif args.algorithm == 'greedy':
        alg = GreedyAlgorithm(buy_only=args.buy_only)
    else:
        raise NotImplementedError

    api.update_securities(folio)

    alg.run(acc)
    assert acc.market_value() == acc.market_value_new()

    table = PrettyTable(["Symbol", "Weight", "Price", "Amount", "Value", "Allocation"])
    for pos in acc.positions:
        table.add_row([pos.security.symbol,
                       "{} ({:.2f}%)".format(pos.weight, pos.weight/acc.total_weight()*100),
                       "$ {:.2f}".format(pos.security.price/100),
                       "{:d} -> {:d} ({:d})".format(pos.amount, pos.amount + pos.delta, pos.delta),
                       "$ {:.2f} -> $ {:.2f} ($ {:.2f})".format(pos.market_value()/100,
                                                                pos.market_value_new()/100,
                                                                (pos.market_value_new() - pos.market_value())/100),
                       "{:.2f}% -> {:.2f}% ({:.2f}%)".format(pos.market_value()/acc.market_value()*100,
                                                             pos.market_value_new()/acc.market_value_new()*100,
                                                             (pos.market_value_new()/acc.market_value_new() - pos.market_value()/acc.market_value())*100)])
    table.add_row(['CASH',
                   "{} ({:.2f}%)".format(acc.cash_weight, acc.cash_weight/acc.total_weight()*100),
                   None,
                   None,
                   "$ {:.2f} -> $ {:.2f} ($ {:.2f})".format(acc.cash/100,
                                                            (acc.cash + acc.cash_delta)/100,
                                                            acc.cash_delta/100),
                   "{:.2f}% -> {:.2f}% ({:.2f}%)".format(acc.cash/acc.market_value()*100,
                                                         (acc.cash + acc.cash_delta)/acc.market_value_new()*100,
                                                         ((acc.cash + acc.cash_delta)/acc.market_value_new() - acc.cash/acc.market_value())*100)])
    print(table)
