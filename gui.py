import os
import sys

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.title = "FolioB"
        self.left = 100
        self.top = 100
        self.width = 900
        self.height = 600
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.main_widget = MainWidget()
        self.setCentralWidget(self.main_widget)

        self.show()


class MainWidget(QWidget):
    def __init__(self, *args, **kwargs):
        super(MainWidget, self).__init__(*args, **kwargs)

        tabs = QTabWidget()
        #abs.setCornerWidget(self, Qt.TopLeftCorner)
        tabs.setTabPosition(QTabWidget.West)
        #tabs.resize(900,600)

        self.tab_1 = QWidget()
        self.tab_2 = QWidget()
        tabs.addTab(self.tab_1, "Portfolio")
        tabs.addTab(self.tab_2, "Securities")

        main_layout = QVBoxLayout(self)
        main_layout.addWidget(tabs)
        self.setLayout(main_layout)

        self.show()

def main():
    app = QApplication(sys.argv)
    app.setApplicationName('FolioB')
    window = MainWindow()
    app.exec_()

if __name__ == '__main__':
    main()
